import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NutAnalysisDetailsComponent } from './nut-analysis-details/nut-analysis-details.component';
import { NutAnalysisMasterComponent } from './nut-analysis-master/nut-analysis-master.component';

const routes: Routes = [
  {
    path: '', component: NutAnalysisMasterComponent
  }, 
  {
    path: 'master', component: NutAnalysisMasterComponent
  }, 
  {
    path: 'detail', component: NutAnalysisDetailsComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NutritionAnalysisRoutingModule { }
