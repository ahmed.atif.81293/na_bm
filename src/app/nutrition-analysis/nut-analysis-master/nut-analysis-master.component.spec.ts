import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutAnalysisMasterComponent } from './nut-analysis-master.component';

describe('NutAnalysisMasterComponent', () => {
  let component: NutAnalysisMasterComponent;
  let fixture: ComponentFixture<NutAnalysisMasterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutAnalysisMasterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutAnalysisMasterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
