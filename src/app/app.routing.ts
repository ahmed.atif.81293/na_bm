import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { NutAnalysisMasterComponent } from './nutrition-analysis/nut-analysis-master/nut-analysis-master.component';

const routes: Routes = [{
  path: '', component: NutAnalysisMasterComponent
},
{
  path: 'nutrition-analysis', loadChildren: () => import('./nutrition-analysis/nutrition-analysis.module').then(m => m.NutritionAnalysisModule)
},

{
  path: '**',
  redirectTo: 'nutrition-analysis',
  pathMatch: 'full'
}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
