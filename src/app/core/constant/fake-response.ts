

export const API_CURRENCY = {
    "success": true,
    "timestamp": 1519296206,
    "base": "EUR",
    "date": "2022-03-08",
    "currencies": [
        'EUR',
        "AUD",
        "CAD",
        "CHF",
        "CNY",
        "GBP",
        "JPY",
        "USD",
        "AED",
        "AMD"]

}
export const API_LATEST = {
    "success": true,
    "timestamp": 1519296206,
    "base": "EUR",
    "date": "2022-03-08",
    "rates": [
        { name: "AUD", rate: 1.566015 },
        { name: "CAD", rate: 1.560132 },
        { name: "CHF", rate: 1.154727 },
        { name: "CNY", rate: 7.827874 },
        { name: "GBP", rate: 0.882047 },
        { name: "JPY", rate: 132.360679 },
        { name: "USD", rate: 1.23396 },
        { name: "AED", rate: 1.23396 },
        { name: "AMD", rate: 1.23396 }
    ]

}
export const API_SYMBOLS = {

    "success": true,
    "symbols": {
        "AED": "United Arab Emirates Dirham",
        "AFN": "Afghan Afghani",
        "ALL": "Albanian Lek",
        "AMD": "Armenian Dram",
    }

}
export const API_CONVERT = {

    "success": true,
    "query": {
        "from": "GBP",
        "to": "JPY",
        "amount": 25
    },
    "info": {
        "timestamp": 1519328414,
        "rate": 148.972231
    },
    "historical": "dd",
    "date": "2018-02-22",
    "result": 3724.305775

}

export const TIME_SERIES = {

    "success": true,
    "timeseries": true,
    "start_date": "2012-05-01",
    "end_date": "2012-05-03",
    "base": "EUR",
    "rates": {
        "2012-05-01": {
            "USD": 1.322891,
            "AUD": 1.278047,
            "CAD": 1.302303
        },
        "2012-05-02": {
            "USD": 1.315066,
            "AUD": 1.274202,
            "CAD": 1.299083
        },
        "2012-05-03": {
            "USD": 1.314491,
            "AUD": 1.280135,
            "CAD": 1.296868
        }
    }

}

export const FLUCTUATION = {
    "success": true,
    "fluctuation": true,
    "start_date": "2018-02-25",
    "end_date": "2018-02-26",
    "base": "EUR",
    "rates": {
        "USD": {
            "start_rate": 1.228952,
            "end_rate": 1.232735,
            "change": 0.0038,
            "change_pct": 0.3078
        },
        "JPY": {
            "start_rate": 131.587611,
            "end_rate": 131.651142,
            "change": 0.0635,
            "change_pct": 0.0483
        },
    }
}