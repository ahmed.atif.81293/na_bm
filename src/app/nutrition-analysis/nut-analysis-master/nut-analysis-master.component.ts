import { Component, OnDestroy, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { forkJoin, Subscription } from 'rxjs';
import { ConvertResponse, ExchangeRate } from 'src/app/core/modal/modal';
import { NutAnalysisService } from '../nut-analysis.service';

@Component({
  selector: 'app-nut-analysis-master',
  templateUrl: './nut-analysis-master.component.html',
  styleUrls: ['./nut-analysis-master.component.css']
})
export class NutAnalysisMasterComponent implements OnInit, OnDestroy {
  showDetail: boolean = false;
  currenciesList: any = [];
  exchangeRateform!: FormGroup;
  exchangeRateObj = {} as ExchangeRate;
  convertResponseObj: ConvertResponse = {} as ConvertResponse;
  latestRates: { name: string, rate: number, result: number }[] = [];
  timeSeries: any
  subscriptions = new Subscription();
  constructor(
    private _nutService: NutAnalysisService,
    private fb: FormBuilder) { }

  ngOnInit(): void {
    this.initForm()
    this._nutService.getCurrencies().subscribe(res => {
      this.currenciesList = res?.currencies

    })
  }
  initForm() {
    this.exchangeRateform = this.fb.group({
      from: ['EUR', [Validators.required]],
      to: ['USD', [Validators.required]],
      amount: [null, [Validators.required]],
    })

  }

  convert() {
    this.exchangeRateObj = this.exchangeRateform.getRawValue()
    console.log(this.exchangeRateObj);
    this.latestRates = []
    this.convertResponseObj = {} as ConvertResponse
    this.subscriptions.add(
      forkJoin([this._nutService.convert(this.exchangeRateObj),
      this._nutService.getlatest(this.exchangeRateObj)]).subscribe(res => {
        if (res && res[0]) {
          this.convertResponseObj = res[0]
        }
        if (res && res[1] && res[1]?.rates) {
          this.latestRates = res[1]?.rates
          this.latestRates = this.latestRates.map(rate => {
            return {
              name: rate.name,
              rate: rate.rate,
              result: rate.rate * (this.convertResponseObj?.query?.amount ? +this.convertResponseObj?.query?.amount : 0)
            }
          })

        }
      })
    )
    // this._nutService.convert(this.exchangeRateObj).subscribe(res => {
    //   this.convertResponseObj = res
    // })
  }

  onMoreDetailClick() {
    this.showDetail = true
    this._nutService.convert(this.exchangeRateObj).subscribe(res => {
      this.convertResponseObj = res
    })
  }
  
  onDetailClick(fromCurr: string, ToCurr: string, Ammount: number) {
    debugger
    this.exchangeRateObj.from = fromCurr ? fromCurr : 'EUR'
    this.exchangeRateObj.to = ToCurr ? ToCurr : 'USD'
    this.exchangeRateObj.amount = Ammount?Ammount:1
    this.exchangeRateform.patchValue(this.exchangeRateObj)
    this.convert()
  }
  ngOnDestroy(): void {
    this.exchangeRateform.reset();
    this.subscriptions.unsubscribe()
  }
}
