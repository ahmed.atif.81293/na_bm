import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NutritionAnalysisRoutingModule } from './nutrition-analysis.routing';
import { NutAnalysisMasterComponent } from './nut-analysis-master/nut-analysis-master.component';
import { NutAnalysisDetailsComponent } from './nut-analysis-details/nut-analysis-details.component';
import { CoreModule } from '../core/core.module';
import { NutAnalysisService } from './nut-analysis.service';
import { HttpClientModule } from '@angular/common/http';
import { NgChartsModule } from 'ng2-charts';


@NgModule({
  declarations: [
    NutAnalysisMasterComponent,
    NutAnalysisDetailsComponent,
  ],
  imports: [
    CommonModule,
    NutritionAnalysisRoutingModule,
    CoreModule,
    HttpClientModule,
    NgChartsModule
    
  ],
  providers:[
    NutAnalysisService
  ]
})
export class NutritionAnalysisModule { }
