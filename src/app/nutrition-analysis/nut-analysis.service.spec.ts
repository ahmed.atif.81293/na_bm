import { TestBed } from '@angular/core/testing';

import { NutAnalysisService } from './nut-analysis.service';

describe('NutAnalysisService', () => {
  let service: NutAnalysisService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(NutAnalysisService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
