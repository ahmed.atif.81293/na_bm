
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { API_LATEST, API_CURRENCY, API_SYMBOLS, API_CONVERT, TIME_SERIES, FLUCTUATION } from '../core/constant/fake-response';


const BaseUrl = environment.BaseUrl;
// const Access_key = 'API_KEY '
const Access_key = '0c009a42ef8dddd20b3fab7eccae38b3'

@Injectable({
  providedIn: 'root'
})
export class NutAnalysisService {
  constructor(private http: HttpClient) { }
  getlatest(parm?: any): Observable<any> {
    //return this.http.get(`${BaseUrl}/latest`, { params: { access_key: Access_key, ...parm } })
    // // let parms = new HttpParams({fromObject:{ access_key: 'value1',...parm }})
    // // return this.http.get(`${BaseUrl}/latest`, { params: parms })
    // // // return this.http.get(`${BaseUrl}/latest?access_key=${Access_key}&callback=MY_FUNCTION`)
    return of(API_LATEST)
  }
  getCurrencies(parm?: any): Observable<any> {
    return of(API_CURRENCY)
  }
  getSymbols(parm?: any): Observable<any> {
    // return this.http.get(`${BaseUrl}/symbols`, { params: { access_key: Access_key, ...parm } })
    return of(API_SYMBOLS)
  }

  convert(parm?: any): Observable<any> {
    // return this.http.get(`${BaseUrl}/convert`, { params: { access_key: Access_key, ...parm } })
    return of(API_CONVERT)
  }
  timeseries(parm?: any): Observable<any> {
    //return this.http.get(`${BaseUrl}/timeseries`, { params: { access_key: Access_key, ...parm } })
    return of(TIME_SERIES)
  }

  fluctuation(parm?: any): Observable<any> {
    // return this.http.get(`${BaseUrl}/fluctuation`, { params: { access_key: Access_key, ...parm } })
    return of(FLUCTUATION)
  }
}




