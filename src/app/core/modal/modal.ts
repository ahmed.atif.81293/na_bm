export interface ExchangeRate {
    from: string,
    to: string,
    amount: number
}
export interface ConvertResponse {
    query: ExchangeRate,
    info: { timestamp: number, rate: number },
    historical: string,
    date: Date,
    result: number
}
