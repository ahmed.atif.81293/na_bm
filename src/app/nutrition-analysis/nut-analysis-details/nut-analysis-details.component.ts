import { Component, Input, OnInit } from '@angular/core';
import { MatLabel } from '@angular/material/form-field';
import { ChartDatasetProperties, ChartOptions, ChartType } from 'chart.js';
import { ConvertResponse } from 'src/app/core/modal/modal';
import { NutAnalysisService } from '../nut-analysis.service';

@Component({
  selector: 'app-nut-analysis-details',
  templateUrl: './nut-analysis-details.component.html',
  styleUrls: ['./nut-analysis-details.component.css']
})
export class NutAnalysisDetailsComponent implements OnInit {
  _convertObj: any
  @Input() set convertObj(value: any) {
    this._convertObj = value
    this.getTimeSerial(this._convertObj.query)
  }
  BarcharData = {
    chartLabels: [],
    dataset: [{
      label: '',
      backgroundColor: '#42A5F5',
      borderColor: '#1E88E5',
      data: []
    }]
  }
  barChartOptions: ChartOptions = {
    responsive: true,
  }
  barChartLabels: MatLabel[] = ['2012-05-01', '2012-05-02', 'CAD'];
  barChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];

  barChartData: any[] = [
    { label: 'USD ', data: [1.322891, 1.278047, 1.302303], backgroundColor: '#42A5F5', borderColor: '#1E88E5' },
    { label: 'AUD ', data: [1.315066, 1.274202, 1.299083],  backgroundColor: '#ca70ca', borderColor: '#1f88E5' },
    { label: 'CAD ', data: [1.314491, 1.280135, 1.296868],  backgroundColor: '#df8d35', borderColor:  '#7CB342' },
  ];
  constructor(private _nutService: NutAnalysisService) {
  }
  ngOnInit(): void {

  }
  getTimeSerial(currency: ConvertResponse) {
    this._nutService.timeseries(currency).subscribe(res => {
    })

  }
}
