import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NutAnalysisDetailsComponent } from './nut-analysis-details.component';

describe('NutAnalysisDetailsComponent', () => {
  let component: NutAnalysisDetailsComponent;
  let fixture: ComponentFixture<NutAnalysisDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NutAnalysisDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NutAnalysisDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
